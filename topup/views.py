from django.shortcuts import render, redirect
from django.views import generic
from django.http import HttpResponse, HttpRequest
import requests
from .models import Topup
from datetime import datetime
import hashlib, hmac, json, os, urllib

dir_ = os.path.dirname(os.path.abspath(__file__))
dir_ = "{0}{1}".format(dir_, "/templates/topup/api_ret.html")

# returns renderable form.
class Topuptemp(generic.TemplateView):
    template_name = "topup/topup.html"

#posts to ipays api
def ipay_post(r):
    key = "demoCHANGED"
    data = {"live":"0",
        "oid":"kenamrt-topup"+str(datetime.now()),
        "ttl":r.GET["amount"],
        "tel":r.GET["phone"],
        "eml":r.GET["email"],
        "vid":"demo",
        "curr":"KES",
        "p1":r.GET["wbid"],
        "cbk":r.build_absolute_uri('/ipay/ipn'),
        "cst":"1",
        "crl":"2"
    }

    text = "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format( data['live'], 
                                                    data['oid'],
                                                    data['ttl'],
                                                    data['tel'],
                                                    data['eml'],
                                                    data['vid'],
                                                    data['curr'],
                                                    data['p1'],
                                                    data['cbk'],
                                                    data['cst'],
                                                    data['crl'])
    
    hashobj = hmac.new(key.encode(), text.encode(), hashlib.sha1)
    hashtxt = hashobj.hexdigest()
    data['hsh'] = hashtxt
    url = "https://payments.ipayafrica.com/v3/ke?" + urllib.urlencode(data)
    return redirect(url)

#validate payment from ipays ipn
def ipay_ipn(r):
    id  = r.GET['id']
    ivm = r.GET['ivm']
    qwh = r.GET['qwh']
    afd = r.GET['afd']
    poi = r.GET['poi']
    uyt = r.GET['uyt']
    ifd = r.GET['ifd']
    ipn_url = "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}".format( 'https://www.ipayafrica.com/ipn/?vendor=demo',
                                                                    '&id',id,'&ivm',ivm,'&qwh',qwh,'&afd',afd,'&poi',poi,'&uyt',uyt,'ifd',ifd )
    rp = requests.get(ipn_url)
    complete = False
    if rp.status_code == 200:
        txn = Topup(wbid=r.GET['p1'], amount=r.GET['mc'], oid=r.GET['ivm'], tpuname=r.GET['msisdn_idnum'], tpphone=r.GET['msisdn_idnum'], txnid=r.GET['id'])
        print(txn)
        txn.save()
        complete = True
        return render(r, 'topup/api_ret.html', context={
            "complete": complete
        })
    else:
        return render(r, 'topup/api_ret.html', context={
            "complete": complete
        })