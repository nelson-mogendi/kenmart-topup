from os import name
from django.conf.urls import url
from .views import Topuptemp, ipay_ipn, ipay_post

urlpatterns = [
    url('topup', Topuptemp.as_view(), name="Topup"),
    url('ipaypost', ipay_post, name="IpayPost"),
    url('ipay/ipn', ipay_ipn, name="IpayIPN"),
]