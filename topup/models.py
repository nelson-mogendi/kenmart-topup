from django.db import models
from django.utils import timezone

# topup table for completed transactions
class Topup(models.Model):
    wbid = models.CharField(max_length=200)
    amount = models.CharField(max_length=200)
    oid = models.CharField(max_length=200)
    tptimestamp = models.DateTimeField(default=timezone.now())
    tpuname = models.CharField(max_length=200)
    tpphone = models.CharField(max_length=12)
    txnid = models.CharField(max_length=300)

    def __str__(self):
        return self.wbid
